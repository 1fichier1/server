BIN=1fichier_downloader_server
INSTALL_DIR?=/usr/local/bin

all: $(BIN)

1fichier_downloader_server:
	go build

.PHONY: install	
install: $(BIN)
	install $(BIN) $(INSTALL_DIR)

.PHONY: clean
clean:
	rm -f $(BIN)

.PHONY: uninstall
uninstall:
	rm -f $(INSTALL_DIR)/$(BIN)