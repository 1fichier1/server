module 1fichier_downloader_server

go 1.15

require (
	github.com/cavaliercoder/grab v2.0.0+incompatible
	github.com/elazarl/goproxy v0.0.0-20201021153353-00ad82a08272 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/parnurzeal/gorequest v0.2.16
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rs/cors v1.7.0
	github.com/smartystreets/goconvey v1.6.4 // indirect
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b // indirect
	moul.io/http2curl v1.0.0 // indirect
)
