package main

import (
	"database/sql"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

// Database struct
type Database struct {
	filename string
	Db       *sql.DB
	Settings map[string]string
}

// NewDatabase creaates a new SQLite databse
func NewDatabase(filename string) (*Database, error) {
	Database := Database{
		filename: filename,
		Settings: make(map[string]string),
	}

	// Open the database
	if err := Database.open(); err != nil {
		return nil, err
	}

	return &Database, nil
}

// open opens a database
func (s *Database) open() error {
	// Create the SQLite file if it doesn't exists
	if !s.exists() {
		s.createFile()
	}

	// Open the SQLite database
	db, err := sql.Open("sqlite3", s.filename)
	if err != nil {
		return err
	}

	s.Db = db

	// Create the inital tables
	return s.CreateTables()
}

// Close closes a database
func (s *Database) Close() error {
	return s.Db.Close()
}

// createFile creates a new database file if it doesn't already exist, otherwise do nothing
func (s *Database) createFile() error {
	if _, err := os.Create(s.filename); err != nil {
		return err
	}

	return nil
}

// exists checks if a database file already exists
func (s *Database) exists() bool {
	if _, err := os.Stat(s.filename); os.IsNotExist(err) {
		return false
	}

	return true
}

// CreateTables creates the initial tables in the database
func (s *Database) CreateTables() error {
	createTablesSQL := `
		CREATE TABLE IF NOT EXISTS settings (
			"field" TEXT PRIMARY KEY,
			"value" TEXT NON NULL
		);
		CREATE TABLE IF NOT EXISTS downloads (
			"id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
			"url" TEXT NON NULL UNIQUE,
			"filename" TEXT NON NULL UNIQUE,
			"bytesComplete" INTEGER NOT NULL DEFAULT 0,
			"size" INTEGER NOT NULL DEFAULT 0,
			"progress" INTEGER NOT NULL DEFAULT 0,
			"ellapsed" INTEGER NOT NULL DEFAULT 0,
			"done" INTEGER NOT NULL DEFAULT 0,
			"paused" INTEGER NOT NULL DEFAULT 0
		);`
	_, err := s.Db.Exec(createTablesSQL)

	return err
}
