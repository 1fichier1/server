package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

// Page struct
type Page struct {
	Title string
}

var settingsManager *SettingsManager
var downloadManager *DownloadManager
var database *Database

// wsRequestHandler create a new Websocket and starts the reading and writing pumps
func wsRequestHandler(w http.ResponseWriter, r *http.Request) {
	webSocket, err := NewWebSocket(downloadManager, w, r)

	if err != nil {
		log.Println("Error creating WebSocket")
	}

	// Start the Websocket reading pump
	go webSocket.ReadPump()

	// Process messages coming from the websocket
	for message := range webSocket.Reader {
		switch message.Action {
		case "update":
			// Client requested an update

			// Update the progress of all downloads
			downloadManager.UpdateProgress()

			// Create and send the update response message
			updateMessage := createUpdateResponseMessage()
			webSocket.Send(updateMessage)
			//webSocket.conn.WriteJSON(updateMessage)
		case "pause":
			// Client requested to pause download
			downloadManager.PauseDownload(message.Data.(string))
		case "resume":
			// Client requested to resume download
			go downloadManager.ResumeDownload(message.Data.(string))
		case "cancel":
			// Client requested to cancel download
			downloadManager.CancelDownload(message.Data.(string), false)
		case "cancel_delete":
			// Client requested to cancel download and delete associated file
			downloadManager.CancelDownload(message.Data.(string), true)
		case "remove":
			// Client requested to cancel download
			downloadManager.RemoveDownload(message.Data.(string), false)
		case "remove_delete":
			// Client requested to cancel download and delete associated file
			downloadManager.RemoveDownload(message.Data.(string), true)
		}
	}
}

func dlRequestHandler(w http.ResponseWriter, r *http.Request) {
	// Get the real 1fichier URL from the client provided URL
	if url, err := GetFileURL(r.PostFormValue("1fichier_url"), settingsManager.Get("apiKey")); err != nil {
		log.Println(err)
	} else {
		log.Printf("Downloading %v...\n", url)

		// Start downloading the file
		go downloadManager.StartDownload(url)

		// Send response to the client
		response := fmt.Sprintf(`{"url": "%v"}`, url)
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(response))
	}
}

// settingsResponseHandler handles  settings requests
func settingsResponseHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		// Create settings from the POST values
		settingsManager.Set("apiKey", r.PostFormValue("apiKey"))
		settingsManager.Set("downloadFolder", r.PostFormValue("downloadFolder"))

		// Save settins to the database
		if err := settingsManager.SaveSettings(); err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Send a 204 response
		w.WriteHeader(http.StatusNoContent)
	} else if r.Method == http.MethodGet {
		// Send response to the client
		response, err := json.Marshal(settingsManager.Settings)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(response)
	}
}

func main() {
	// Create a channel to receive signals
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		sig := <-sigs
		if sig == syscall.SIGINT || sig == syscall.SIGTERM {
			os.Exit(0)
		}
	}()

	// Get server listening port from command line arguments
	var serverPort = flag.Int("port", 4000, "Server listening port")
	flag.Parse()

	// Open database connection
	database, err := NewDatabase("./database.sqlite")
	if err != nil {
		log.Fatalln(err)
	}
	defer database.Close()

	// Load settings
	settingsManager = NewSettingsManager(database)
	if err := settingsManager.LoadSettings(); err != nil {
		log.Fatalln(err)
	}

	// Create a new download manager and load the downloads currently in Database
	downloadManager = NewDownloadManager(settingsManager.Get("downloadFolder"), 500, database)
	if err := downloadManager.LoadDownloads(); err != nil {
		log.Fatalln(err)
	}

	log.Printf("Starting server on port %d...\n", *serverPort)

	// Create the server and handles calls
	r := mux.NewRouter()
	r.HandleFunc("/ws", wsRequestHandler)
	r.HandleFunc("/dl", dlRequestHandler)
	r.HandleFunc("/settings", settingsResponseHandler).Methods("GET", "POST")

	handler := cors.Default().Handler(r)
	http.ListenAndServe(fmt.Sprintf(":%d", *serverPort), handler)
}

// createUpdateResponseMessage creates an update response message to be sent to client
func createUpdateResponseMessage() WebsocketOutMessage {
	wsUpdate := updateMessage{}

	for _, d := range downloadManager.activeDownloads {
		if d.URL == "" {
			continue
		}

		wsUpdate.ActiveDownloads = append(wsUpdate.ActiveDownloads, d)
	}

	if _, err := json.Marshal(WebsocketOutMessage{
		Action: "update",
		Data:   wsUpdate,
	}); err != nil {
		log.Println(err)
	}

	return WebsocketOutMessage{
		Action: "update",
		Data:   wsUpdate,
	}
}
